class Customer < ApplicationRecord
  has_many :orders, dependent: :destroy
  validates :name, presence: true,
            length: { minimum: 4 }
  validates :email, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
end
