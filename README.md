# Uebung2

Ein Ruby on Rails Projekt, welches "Hello World!" und "Goodbye!" in versch. Sprachen ausgibt (Nicht-ASCII-Zeichensatz).

## Vorraussetzungen

* Ruby 2.4.1
* Rails 5.1.4

## Erste Schritte

1. gem installieren mit `sudo apt-get install gem`
2. rails installieren mit `gem install rails -v 5.1.4`
3. rvm und ruby installieren mit 

	* `curl -sSL https://rvm.io/mpapis.asc | gpg --import -`
	* `\curl -sSL https://get.rvm.io | bash -s stable --ruby=2.4.1`
	* `source ~/.rvm/scripts/rvm`
	
## Testen des Projekts

1. Projekt clonen
2. `bundle install` in ``/your/path/webentwicklungabgaben`` ausfuehren, damit alle noetigen gems installiert werden 
2. Im Terminal `rails server` oder `rails s` in ``/your/path/uebung2`` ausfuehren um den Server zu starten
3. Im Browser ``localhost:3000`` eingeben, um auf die Startseite zu kommen (Hello World!)
4. Im Browser ``localhost:3000/goodbye`` eingeben, um auf die Goodbye-Seite zu kommen
5. Zum Schluss den Server stoppen mit `ctrl+c`   

## Autor

* **Ugur Koese** 

# Uebung3

Ein Ruby on Rails Projekt, welches die Möglichkeit bietet Customers(Kunden) zu erstellen, bearbeiten und zu löschen. Außerdem kann man zu jedem Customer Orders(Aufträge) hinzufügen. 

## Vorraussetzungen

* Ruby 2.4.1
* Rails 5.1.4

## Testen des Projektes

1. siehe obige 5 Schritte
2. `rails db:migrate` in ``/your/path/webentwicklungabgaben`` ausfuehren, damit die Datenbank migriert wird
3. In ``localhost:3000`` auf ``Show Customers`` klicken, um alle Customers aus der Datenbank auszugeben
4. Auf ``New Customer`` klicken, um einen neuen Customer zu erstellen
	1. ``Name`` und ``Email`` sind Pflichtfelder, d.h. sie müssen ausgefüllt werden 
5. Eine Order hinzufügen 
6. Mit `Edit` Attribute eines Customers verändern und aktualisieren
7. Mit `Show` Attribute und Orders eines Customers anzeigen
8. Mit `Destroy` den Customer aus der Datenbank löschen
	1. Dadurch wird die Order auch automatisch mit gelöscht
9. Mit `Destroy Order` eine Order löschen
	1. Option nur zu sehen, wenn es eine Order gibt
	2. Customer wird nicht gelöscht


## Autor

* **Ugur Koese** 

- - - -
***Zum Erstellen/Löschen/Verändern wird ein Name & ein Passwort gebraucht***

>> *Name: admin & Passwort: admin* 	

- - - - 