Rails.application.routes.draw do
    get 'welcome/index'
    get 'goodbye', to: 'goodbye#goodbye'

    resources :customers do
      resources :orders
    end

    root 'welcome#index'
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
